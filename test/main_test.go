package test

import (
	"context"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"gitlab.com/jackyveshx/go-web-common-simplecurd/dynamic"
	"gitlab.com/jackyveshx/go-web-common-simplecurd/model"
	routers "gitlab.com/jackyveshx/go-web-common-simplecurd/router"
	"net/http"
	"os"
	"os/signal"
	"testing"
	"time"
)

type SqlEntity struct {
	model.BasicEntity
	Description string `gorm:"column:description" json:"description"`
	SqlText string `gorm:"column:sql_text" json:"sqlText"`
}

func (SqlEntity) TableName() string {
	return "sql_entity"
}

func (SqlEntity) GetKey() string {
	return "sqlEntity"
}

func (SqlEntity) DoQuery(runner dynamic.SimpleDBRunner) (interface{},int64) {
	var list []SqlEntity
	runner.GetQueryDB().Find(&list)
	return list, runner.TotalCount(SqlEntity{})
}

func (SqlEntity) DoCreate(gdb *gorm.DB, b []byte) error {
	a := SqlEntity{}
	err := json.Unmarshal(b,&a)
	if err != nil {
		return err
	}
	return gdb.Create(&a).Error
}

func (SqlEntity) DoUpdate(gdb *gorm.DB, b []byte) error {
	a := SqlEntity{}
	err := json.Unmarshal(b,&a)
	if err != nil {
		return err
	}
	return gdb.Save(&a).Error
}

func TestRouter(t *testing.T) {
	router := gin.Default()

	router.Use(gin.Logger())
	router.Use(gin.Recovery())
	routers.InitSimpleCrudRouter(router)

	model.InitDb(0,"mysql","192.168.50.34","3306","hadoop_dw","cdh","CDH360lj#com2019","")
	model.InitSimpleCurcEntities(SqlEntity{})

	s := &http.Server{
		Addr: ":8090",
		Handler: router,
		ReadTimeout: 60 * time.Second,
		WriteTimeout: 60 * time.Second,
		//MaxHeaderBytes: 1 << 20, //我觉得这个还是没有必要，
	}
	//另一个协程去启动和监听
	go func() {
		if err := s.ListenAndServe(); err != nil {
			t.Logf("Listen: %s\n", err)
		}
	}()

	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	t.Log("Shutdown Server ...")

	model.Close()
	ctx, cancel := context.WithTimeout(context.Background(), 5 * time.Second)
	defer cancel()
	if err := s.Shutdown(ctx); err != nil {
		t.Logf("Server Shutdown:%s\n", err)
	}
	t.Logf("Server exiting\n")
}