package router

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/jackyveshx/go-web-common-simplecurd/dynamic"
	"gitlab.com/jackyveshx/go-web-common-simplecurd/model"
	"log"
	"net/http"
	"strconv"
	"strings"
)

const (
	_DynamicParams = "dynamicParams"
)

//初始化路由组件
func InitSimpleCrudRouter(rootEngin *gin.Engine) {
	simple := rootEngin.Group("/crud/api")
	simple.Use(dynamicParamMiddleware)

	simple.GET("/:model/page", simpleQueryFunc)
	simple.POST("/:model",simpleCreateFunc)
	simple.PUT("/:model",simpleUpdateFunc)
	simple.DELETE("/:model/:id",simpleDeleteFunc)
}

//中间剪，用于解析url成DynamicParam
func dynamicParamMiddleware(c *gin.Context) {
	var err error
	queryMap := c.Request.URL.Query()
	params := dynamic.DynamicParams{}
	var filterMap = make(map[string]*dynamic.DynamicFilter)
	var operateTypeMap = make(map[string]dynamic.OperateType)
	for k, v := range queryMap {
		if strings.HasSuffix(k,"Operate") {
			key := strings.Replace(k,"Operate","",-1)
			if operateType,err1 := dynamic.GetOperaType(v[0]); err1 != nil {
				err = err1
				break
			} else {
				operateTypeMap[key] = operateType
			}
		} else {
			if k == "page" {
				if params.Page, err = strconv.Atoi(v[0]); err != nil {
					break
				}
			} else if k == "size" {
				if params.Size, err = strconv.Atoi(v[0]); err != nil {
					break
				}
			} else if k == "sortDirection" {
				sortDirection := dynamic.GetSortDirection(v[0])
				params.SortDirection = sortDirection
			} else if k == "sortField" {
				params.SortField = v[0]
			} else {
				filter := dynamic.DynamicFilter{
					Field:k,
					Operate:dynamic.Equal, //默认是Equal
					Value:v[0],
				}
				filterMap[k] = &filter
			}
		}
	}

	if err != nil {
		c.JSON(http.StatusNotAcceptable,gin.H{
			"msg":"parse param error",
		})
		c.Abort()
	}

	for k, v := range operateTypeMap {
		if e,ok := filterMap[k]; ok {
			e.Operate = v
		}
	}
	filterList := make([]dynamic.DynamicFilter,0)
	for _,v := range filterMap {
		filterList = append(filterList,*v)
	}
	params.Filters = filterList
	c.Set(_DynamicParams,params)
	c.Next()
}

//动态参数分页查询
func simpleQueryFunc(c *gin.Context) {
	key := c.Param("model")
	crudEntity,err := model.GetCurdEntity(key)
	if err != nil {
		c.JSON(http.StatusNotFound,gin.H{})
		return
	}

	var params dynamic.DynamicParams
	if paramsIntf, exist := c.Get(_DynamicParams); !exist {
		params = dynamic.DynamicParams{}
	} else {
		params = paramsIntf.(dynamic.DynamicParams)
	}

	simpleCurdRunner, err := params.ToSimpleDBRunner(model.GetDb())
	if err != nil {
		panic(err)
	}

	list,count := crudEntity.DoQuery(simpleCurdRunner)
	c.JSON(http.StatusOK, gin.H{
		"item":list,
		"totalCount":count,
	})
}

//动态参数新增
func simpleCreateFunc(c *gin.Context) {
	key := c.Param("model")
	crudEntity,err := model.GetCurdEntity(key)
	if err != nil {
		c.JSON(http.StatusNotFound,gin.H{})
		return
	}

	bodyData, err := c.GetRawData()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"msg":"get request body error, maybe the request is not [POST] ",
		})
	}

	if err = crudEntity.DoCreate(model.GetDb(),bodyData); err != nil {
		log.Println(err)
		c.JSON(http.StatusInternalServerError, gin.H{
			"msg":err.Error(),
		})
	} else {
		c.JSON(http.StatusOK, gin.H{
			"msg":"save success",
		})
	}
}

//动态参数更新
func simpleUpdateFunc(c *gin.Context) {
	key := c.Param("model")
	crudEntity,err := model.GetCurdEntity(key)
	if err != nil {
		c.JSON(http.StatusNotFound,gin.H{})
		return
	}

	bodyData, err := c.GetRawData()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"msg":"get request body error, maybe the request is not [POST] ",
		})
	}

	if err = crudEntity.DoUpdate(model.GetDb(),bodyData); err != nil {
		log.Println(err)
		c.JSON(http.StatusInternalServerError, gin.H{
			"msg":err.Error(),
		})
	} else {
		c.JSON(http.StatusOK, gin.H{
			"msg":"save success",
		})
	}
}

func simpleDeleteFunc(c *gin.Context) {
	key := c.Param("model")
	crudEntity,err := model.GetCurdEntity(key)
	if err != nil {
		c.JSON(http.StatusNotFound,gin.H{})
		return
	}
	id := c.Param("id")
	where := fmt.Sprintf("%s = ?", id)
	idValue := c.Query(id)

	if err := model.GetDb().Table(crudEntity.TableName()).Where(where,idValue).Delete(&crudEntity).Error; err != nil {
		log.Println(err)
		c.JSON(http.StatusInternalServerError, gin.H{
			"msg":err.Error(),
		})
	} else {
		c.JSON(http.StatusOK, gin.H{
			"msg":"delete success",
		})
	}
}