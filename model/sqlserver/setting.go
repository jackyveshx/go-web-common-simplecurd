package sqlserver

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mssql"
	"github.com/prometheus/common/log"
)

func Initdb(host, port, dbname, user, password, options string) (*gorm.DB) {
	var url string
	if options != "" {
		url = fmt.Sprintf("sqlserver://%s:%s@%s:%s?database=%s&%s",
			user,password,host,port,dbname,options)
	} else {
		url = fmt.Sprintf("sqlserver://%s:%s@%s:%s?database=%s",
			user,password,host,port,dbname)
	}

	db, err := gorm.Open("mssql", url)
	if err != nil {
		log.Fatalf("init db error: %v\n",err)
	}
	db.LogMode(true)
	return db
}