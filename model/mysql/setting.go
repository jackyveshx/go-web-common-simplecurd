package mysql

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/prometheus/common/log"
)

func Initdb(host, port, dbname, user, password, options string) (*gorm.DB) {
	var url string
	if options != "" {
		url = fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?%s",
			user,password,host,port,dbname,options)
	} else {
		url = fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local",
			user,password,host,port,dbname)
	}

	db, err := gorm.Open("mysql", url)
	if err != nil {
		log.Fatalf("init db error: %v\n",err)
	}
	db.LogMode(true)
	return db
}
