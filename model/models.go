package model

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"gitlab.com/jackyveshx/go-web-common-simplecurd/dynamic"
	"time"
)

//基本类型
type BasicEntity struct {
	Id string `gorm:"primary_key;column:id" json:"id"`
	CrtDate time.Time `gorm:"column:crt_date" json:"crtDate"`
	LastUpdate time.Time `gorm:"column:last_update" json:"lastUpdate"`
	//Version int64 `gorm:"column:version" json:"version"`
}

//新增时赋值时间戳
func (b *BasicEntity) BeforeCreate() error {
	b.Id = fmt.Sprintf("%d",snowflakeNode.Generate())
	b.CrtDate = time.Now()
	b.LastUpdate = time.Now()
	return nil
}

//更新前赋值时间戳
func (b *BasicEntity) BeforeUpdate() error {
	b.LastUpdate = time.Now()
	return nil
}

//传入SimpleDBRunner来执行
type SimpleCrudEntity interface {
	GetKey() string
	TableName() string
	DoQuery(dynamic.SimpleDBRunner) (interface{}, int64)
	DoCreate(*gorm.DB, []byte) error
	DoUpdate(*gorm.DB, []byte) error
}

var _entities []SimpleCrudEntity

//初始化Entity，用于后面责任链执行
func InitSimpleCurcEntities(entity... SimpleCrudEntity) {
	_entities = entity
}

func GetCurdEntity(key string) (SimpleCrudEntity, error) {
	for _, entity := range _entities {
		if entity.GetKey() == key {
			return entity,nil
		}
	}
	return nil,fmt.Errorf("Entity of %s not found. ",key)
}