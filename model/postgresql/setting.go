package postgresql

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/prometheus/common/log"
)

func Initdb(host, port, dbname, user, password, options string) (*gorm.DB) {
	var url string
	if options != "" {
		url = fmt.Sprintf("user=%s password=%s host=%s port=%s dbname=%s %s",
			user,password,host,port,dbname,options)
	} else {
		url = fmt.Sprintf("user=%s password=%s host=%s port=%s dbname=%s",
			user,password,host,port,dbname)
	}

	db, err := gorm.Open("postgres", url)
	if err != nil {
		log.Fatalf("init db error: %v\n",err)
	}
	db.LogMode(true)
	return db
}