package model

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/jackyveshx/go-web-common-simplecurd/model/mysql"
	"gitlab.com/jackyveshx/go-web-common-simplecurd/model/postgresql"
	"gitlab.com/jackyveshx/go-web-common-simplecurd/model/sqlserver"
	"gitlab.com/jackyveshx/go-web-common-simplecurd/util"
	"log"
)

var (
	db *gorm.DB
	snowflakeNode *util.SnowFlakeNode
	multiDb map[string]*gorm.DB
)

//创建数据库连接层
func InitDb(snowflakeId int64, kind, host, port, dbname, user, password, options string) {
	switch kind {
	case "mysql":
		db = mysql.Initdb(host,port,dbname,user,password,options)
	case "postgresql":
		db = postgresql.Initdb(host,port,dbname,user,password,options)
	case "mssql":
		db = sqlserver.Initdb(host,port,dbname,user,password,options)
	default:
		log.Fatalf("db type %s not supported yet.",kind)
	}

	//初始化雪花算法的节点
	var err error
	snowflakeNode, err = util.NewSnowFlakeNode(snowflakeId)
	if err != nil {
		log.Fatalf("init snowflake worker error: %s", err)
	}
}

//根据类型创建db
func CreateDb(kind, host, port, dbname, user, password, options string) (*gorm.DB) {
	switch kind {
	case "mysql":
		return mysql.Initdb(host,port,dbname,user,password,options)
	case "postgresql":
		return postgresql.Initdb(host,port,dbname,user,password,options)
	case "mssql":
		return sqlserver.Initdb(host,port,dbname,user,password,options)
	default:
		log.Fatalf("db type %s not supported yet.",kind)
		return nil
	}
}

//创建多数据源的db
func InitMultiDb(snowflakeId int64,maps map[string]*gorm.DB) {
	multiDb = maps

	//初始化雪花算法的节点
	var err error
	snowflakeNode, err = util.NewSnowFlakeNode(snowflakeId)
	if err != nil {
		log.Fatalf("init snowflake worker error: %s", err)
	}
}

func GetDb() (*gorm.DB) {
	return db
}

func GetDbBy(key string) (*gorm.DB) {
	if v,ok := multiDb[key]; ok {
		return v
	} else {
		log.Fatalf("get db err, no such db exists: %s, please check when init.", key)
	}
	return nil
}

//关闭数据库链接
func Close() {
	var err error
	if db != nil {
		err = db.Close()
		if err != nil {
			log.Fatal(err)
		}
	}
	for _,v := range multiDb {
		if v != nil {
			err = v.Close()
			if err != nil {
				log.Fatal(err)
			}
		}
	}
}