# go-web-common-simplecurd

a golang simple curd module with gorm and gin

一个简单的通用接口型crud的组件。

## Usage
URL Api:

* GET `/crud/api/:model/page` 根据动态条件分页获取
* POST `/crud/api/:model` 添加
* PUT `/crud/api/:model` 更新
* DELETE `/crud/api/:model/:id?{:id}=` 删除

### 动态查询的usage:

* 基本分页：
    `GET {url}?page=1&size=10`
* 基本分页排序：
    `GET {url}?page=1&size=10&sortField=crtDate&sortDirection=DESC` 
    `sortField` 是需要排序的字段 `sortDirection` 是升降序
* 基本带参数查询：
    `GET {url}?name=yourname&age=20&...` 
    非关键字（上面两条中的）的参数，直接视为等于的操作。
* 不同类型的参数查询：
    `GET {url}?age=20&ageOperate=LessEqual&...`
    使用 `字段名+Operate` 的参数来表示判断类型：
    
    
| 操作类型 | 说明 |
| :---: | :---: |
| Equal | 等于（默认）|
| NotEqual| 不等于 |
| Less | （数字类型）小于 |
| LessEqual | （数字类型）不大于 |
| Greater | （数字类型）大于 |
| GreaterEqual | （数字类型）不小于 |
| Contains | （字符串）包含 |
| StartWith | （字符串）以开头 |
| EndWith | （字符串）以结尾 |


### 开发使用

本组件其实已经包含了web端组件gin和orm组件gorm，在gorm层侵略不算很大，若是不同的web框架则不推荐。

另外，gorm层上，仅包含了分页查询、全结构新增、全结构更新和where条件删除，如果不能满足可以自行实现，不用该组件。

1、首先，数据库映射对象需要实现`SimpleCrudEntity`这个接口:

```go
type SimpleCrudEntity interface {
	GetKey() string
	TableName() string
	DoQuery(dynamic.SimpleDBRunner) (interface{}, int64)
	DoCreate(*gorm.DB, []byte) error
	DoUpdate(*gorm.DB, []byte) error
}
```

其中：`GetKey()`是返回对应的URL上的`:model`参数值。
`DoCreate`和`DoUpdate`中的字节数组是用反序列化json成对象的。
`dynamic.SimpleDBRunner`的函数仅有两个：

```go
//获取已经拼接好的查询条件和分页信息
func (r SimpleDBRunner) GetQueryDB() *gorm.DB {
	return r.db
}

//获取已经拼接好的查询条件的所有数量
func (r SimpleDBRunner) TotalCount(out interface{}) int64 {
	var count int64
	r.dbc.Model(out).Count(&count)
	return count
}
```

2、在gin路由器中传入，初始化crud的路由

```go
//...
router := gin.Default()
router.Use(gin.Logger()) //gin的日志中间件
router.Use(gin.Recovery()) //gin的panic回复中间件

//初始化动态crud路由
routers.InitSimpleCrudRouter(router)
//...
```

然后需要初始化DB和注册对应的数据库映射对象，同时需要赋值雪花算法的id：

```go
model.InitDb(0,"mysql","ip","port","database","user","password","")
model.InitSimpleCurcEntities(Table1{}, Table2{},Table3{}...)
```

最后启动你的gin即可