package dynamic

import (
	"encoding/json"
	"fmt"
	"github.com/jinzhu/gorm"
	"strings"
)

//判断类型的枚举
type OperateType int

const (
	Equal OperateType = iota
	NotEqual
	Less
	LessEqual
	Greater
	GreaterEqual
	Contains
	StartWith
	EndWith
)

func GetOperaType(key string) (OperateType,error) {
	switch key {
	case "Equal","equal","EQ","eq":
		return Equal,nil
	case "NotEqual","notequal","NE","ne":
		return NotEqual,nil
	case "Less", "less", "LT", "lt":
		return Less,nil
	case "LessEqual","lessequal","LE","le":
		return LessEqual,nil
	case "Greater","greater","gt","GT":
		return Greater,nil
	case "GreaterEqual", "greaterequal", "GE", "ge":
		return GreaterEqual,nil
	case "Contains", "contains":
		return Contains,nil
	case "StartWith","startwith":
		return StartWith,nil
	case "EndWith", "endwith":
		return EndWith,nil
	default:
		err := fmt.Errorf("can not unmarshal json with %s\n",key)
		return -1, err
	}
}

//判断类型反序列化
func (a *OperateType) UnmarshalJSON(b []byte) error {
	var s string
	if err := json.Unmarshal(b, &s); err != nil {
		return err
	}
	switch s {
	case "Equal","equal","EQ","eq":
		*a = Equal
	case "NotEqual","notequal","NE","ne":
		*a = NotEqual
	case "Less", "less", "LT", "lt":
		*a = Less
	case "LessEqual","lessequal","LE","le":
		*a = LessEqual
	case "Greater","greater","gt","GT":
		*a = Greater
	case "GreaterEqual", "greaterequal", "GE", "ge":
		*a = GreaterEqual
	case "Contains", "contains":
		*a = Contains
	case "StartWith","startwith":
		*a = StartWith
	case "EndWith", "endwith":
		*a = EndWith
	default:
		err := fmt.Errorf("can not unmarshal json with %s\n",s)
		return err
	}
	return nil
}

//序列化判断类型
func (a OperateType) MarshalJSON() ([]byte, error) {
	var s string
	switch a {
	case Equal:
		s = "Equal"
	case NotEqual:
		s = "NotEqual"
	case Less:
		s = "Less"
	case LessEqual:
		s = "LessEqual"
	case Greater:
		s = "Greater"
	case GreaterEqual:
		s = "GreaterEqual"
	case Contains:
		s = "Contains"
	case StartWith:
		s = "StartWith"
	case EndWith:
		s = "EndWith"
	default:
		err := fmt.Errorf("can not marshal unknown type:%v\n",a)
		return nil, err
	}
	return json.Marshal(s)
}







//用于存放sql和其value的结构
type SqlWithValue struct {
	Sql string
	Value string
}

//条件过滤字段
type DynamicFilter struct {
	Field string `json:"field"`
	Operate OperateType `json:"operate"`
	Value string `json:"value"`
}

//条件过滤字段转成gorm的where条件
func (filter DynamicFilter) ToWhereCondition() (SqlWithValue, error) {
	var sql string
	switch filter.Operate {
	case Equal:
		sql = fmt.Sprintf("%s = ?", filter.Field)
	case NotEqual:
		sql = fmt.Sprintf("%s <> ?", filter.Field)
	case Less:
		sql = fmt.Sprintf("%s < ?", filter.Field)
	case LessEqual:
		sql = fmt.Sprintf("%s <= ?", filter.Field)
	case Greater:
		sql = fmt.Sprintf("%s > ?", filter.Field)
	case GreaterEqual:
		sql = fmt.Sprintf("%s >= ?", filter.Field)
	case Contains:
		sql = fmt.Sprintf("%s like ?", filter.Field)
		filter.Value = strings.Join([]string{"%",filter.Value,"%"},"")
	case StartWith:
		sql = fmt.Sprintf("%s like ?", filter.Field)
		filter.Value = strings.Join([]string{filter.Value,"%"},"")
	case EndWith:
		sql = fmt.Sprintf("%s like ?", filter.Field)
		filter.Value = strings.Join([]string{"%",filter.Value},"")
	default:
		err := fmt.Errorf("can not parse with opera %s\n", filter.Operate)
		return SqlWithValue{}, err
	}

	return SqlWithValue{sql,filter.Value}, nil
}









//排序
type SortDirection bool

const (
	ASC SortDirection = false
	DESC SortDirection = true
)

func GetSortDirection(key string) SortDirection {
	switch key {
	case "DESC","desc":
		return DESC
	case "ASC","asc":
		return ASC
	default:
		err := fmt.Errorf("can not parse SortDirection with : %s",key)
		panic(err)
	}
}

func (a SortDirection) String() string {
	if a {
		return "DESC"
	} else {
		return "ASC"
	}
}

func (a *SortDirection) UnmarshalJSON(b []byte) error {
	var s string
	if err := json.Unmarshal(b,&s); err != nil {
		return err
	}
	switch s {
	case "ASC","asc":
		*a = ASC
	case "DESC", "desc":
		*a = DESC
	default:
		err := fmt.Errorf("can not unmarshal json with %s\n",s)
		return err
	}
	return nil
}

func (a SortDirection) MarshalJSON() ([]byte, error) {
	var s string
	switch a {
	case ASC:
		s = "ASC"
	case DESC:
		s = "DESC"
	default:
		err := fmt.Errorf("can not marshal unknown type:%v\n",a)
		return nil,err
	}
	return json.Marshal(s)
}


//封装一下这个gorm.DB，用于构造完条件之后拿来执行的
type SimpleDBRunner struct {
	db *gorm.DB //直接查的
	dbc *gorm.DB //查数量的
}

//封装一下gorm DB的Find
func (r SimpleDBRunner) GetQueryDB() *gorm.DB {
	return r.db
}

func (r SimpleDBRunner) TotalCount(out interface{}) int64 {
	var count int64
	r.dbc.Model(out).Count(&count)
	return count
}

//动态查询参数结构
type DynamicParams struct {
	Filters []DynamicFilter `json:"filters"`
	Page int `json:"page"`
	Size int `json:"size"`
	SortDirection SortDirection `json:"sortDirection"`
	SortField string `json:"sortField"`
}

//奖动态查询参数转成gorm的条件
func (params DynamicParams) ToSimpleDBRunner(db *gorm.DB) (SimpleDBRunner,error) {
	var mydbc = db
	//构造Where条件
	for _, v := range params.Filters {
		if t, err := v.ToWhereCondition(); err != nil {
			return SimpleDBRunner{},err
		} else {
			mydbc = mydbc.Where(t.Sql,t.Value)
		}
	}

	var mydb = mydbc
	//构造分页
	if params.Page != 0 && params.Size != 0 {
		offset := (params.Page - 1) * params.Size
		mydb = mydb.Limit(params.Size).Offset(offset)
	}
	//排序
	if params.SortField != "" {
		order := fmt.Sprintf("%s %s", params.SortField, params.SortDirection.String())
		mydb = mydb.Order(order)
	}
	return SimpleDBRunner{mydb,mydbc}, nil
}